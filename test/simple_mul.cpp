#include <iostream>
#include "../spec_rt/spec_rt.hpp"

int main(int argc, char *argv[])
{
    SpecRuntime srt(argv[1]);
    srt.init();
    SpecPointGeneral *g = dynamic_cast<SpecPointGeneral*>(srt.ctx.getPoint("a"));
    SpecPointRange *r = dynamic_cast<SpecPointRange*>(srt.ctx.getPoint("b"));


    int (*f)(int, int) = (int (*)(int, int)) srt.getFunction("f");
    std::cout << "Calling original version of f(a, b) = a * b ..." << std::endl;
    std::cout << "f(4, 2) = " << f(4, 2) << std::endl;

    // enable instrumentation and re-generate
    std::cout << std::endl << "Enabling instrumentation..." << std::endl;
    g->instrEnable();
    r->instrEnable();
    srt.update();
    f = (int (*)(int, int)) srt.getFunction("f");
    std::cout << "f(4, 2) = " << f(4, 2) << std::endl;
    std::cout << "f(4, 3) = " << f(4, 3) << std::endl;

    std::cout << std::endl << "Looking at monitored values..." << std::endl;
    std::cout << "b.histogram[2] = " << r->histogram[2] << std::endl;
    std::cout << "a.stats[4] = " << g->stats[4] << std::endl;

    // specialize and regenerate
    std::cout << std::endl << "Specializing b = 2..." << std::endl;
    r->specialize(2);
    r->instrDisable();
    srt.update();
    f = (int (*)(int, int)) srt.getFunction("f");
    std::cout << "f(4, 2) = " << f(4, 2) << std::endl;
    std::cout << "a.stats[4] = " << g->stats[4] << std::endl;
    std::cout << std::endl << "Triggering case outside of specialization... (results in abort)" << std::endl;
    std::cout << std::flush;
    std::cout << "f(4, 3) = " << f(4, 3) << std::endl;

    return 0;
}
