#include <stdint.h>

extern int32_t __instr_range_i32(const char *lbl, int32_t x, int32_t l, int32_t h);
extern int32_t __instr_general_i32(const char *lbl, int32_t x);


int f(int a, int b) {
    return __instr_general_i32("a", a) * __instr_range_i32("b", b, 0, 8);
}
