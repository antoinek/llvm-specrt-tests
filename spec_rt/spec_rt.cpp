#include "spec_rt.hpp"
#include "instrument.hpp"

#include <iostream>
#include <stdlib.h>

#include "llvm/IRReader/IRReader.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Passes/PassBuilder.h"
#include "llvm/Transforms/IPO/AlwaysInliner.h"

using namespace llvm;
using namespace llvm::orc;

SpecRuntime::SpecRuntime(const std::string &path_)
    : path(path_), verbose(false)
{
    
}

void SpecRuntime::init()
{
    InitializeNativeTarget();
    InitializeNativeTargetAsmPrinter();
    InitializeNativeTargetAsmParser();

    llvm::sys::DynamicLibrary::LoadLibraryPermanently(nullptr);
    update();
}

void SpecRuntime::update()
{
    i = new Instance;
    i->tsctx = new ThreadSafeContext(std::make_unique<LLVMContext>());

    // Try to detect the host arch and construct an LLJIT instance.
    auto jit_ = LLJITBuilder().create();
    if (!jit_) {
        errs()
         << "error:";
        errs() << jit_.takeError() << "\n";

        errs() << "creating jit for some reason failed\n";
        abort();
    }
    i->jit = std::move(jit_.get());

    // make symbols form this process available in jit
    auto &JD = i->jit->getMainJITDylib();
    const DataLayout &DL = i->jit->getDataLayout();

    std::cerr << "global prefix: '" << DL.getGlobalPrefix() << "'" << std::endl;
    JD.addGenerator(cantFail(DynamicLibrarySearchGenerator::GetForCurrentProcess(
      DL.getGlobalPrefix())));

    // prepare opt pass pipeline
    SMDiagnostic err;
    LoopAnalysisManager LAM;
    FunctionAnalysisManager FAM;
    CGSCCAnalysisManager CGAM;
    ModuleAnalysisManager MAM;
    PassBuilder pb;
    pb.registerModuleAnalyses(MAM);
    pb.registerCGSCCAnalyses(CGAM);
    pb.registerFunctionAnalyses(FAM);
    pb.registerLoopAnalyses(LAM);
    pb.crossRegisterProxies(LAM, FAM, CGAM, MAM);
    pb.registerPipelineStartEPCallback([&](ModulePassManager &MPM) {
        Instrument i;
        i.specCtx = &ctx;
        MPM.addPass(i);
        MPM.addPass(AlwaysInlinerPass());
    });
    ModulePassManager MPM = pb.buildPerModuleDefaultPipeline(PassBuilder::OptimizationLevel::O2);

    // load module
    std::unique_ptr<Module> mod(parseIRFile(path, err, *i->tsctx->getContext()));

    // optimize module
    MPM.run(*mod, MAM);

    if (verbose)
        mod->print(errs(), nullptr);

    // Add the module.
    if (auto Err = i->jit->addIRModule(JD, ThreadSafeModule(std::move(mod), *i->tsctx))) {
        errs() << "adding module to jit failed\n";
        abort();
    }
}

void *SpecRuntime::getFunction(const std::string &fnName)
{
    auto entrySym = i->jit->lookup(fnName);
    if (!entrySym)
        return nullptr;
    return (void *) entrySym->getAddress();
}

// invoke by code in jit when specialization fails. Must not return
void __spec_check_fail(SpecPointBase *e, uint64_t val)
{
    std::cerr << "Specialization for point '" << e->label() << "' failed, "
        "value is " << val << std::endl;
    abort();
}