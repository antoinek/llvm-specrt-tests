#pragma once

#include "llvm/Pass.h"
#include "llvm/IR/Attributes.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/PassManager.h"



#include <map>
#include <string>

#include "spec_ctx.hpp"


/* LLVM pass for adding instrumentation and specialization based on context.
   Initially this can be run with an empty context and will add all points it
   finds and will not enable instrumentation or specialization for any of them.
   specCtx can be overwritten with a pre-initialized context. */
struct Instrument : public llvm::ModulePass {
  static char ID;
  
  SpecCtx staticCtx;
  SpecCtx *specCtx;
  Instrument() : llvm::ModulePass(ID), specCtx(&staticCtx) {}

  Instrument(const Instrument &i) : llvm::ModulePass(ID),
    staticCtx(i.staticCtx), specCtx(i.specCtx) {}


  static llvm::StringRef name();

  bool rewriteInstrCall(llvm::Module &mod, llvm::Function &parent,
    llvm::CallInst &ci);
  bool runOnFunction(llvm::Module &m, llvm::Function &f);
  bool runOnModule(llvm::Module &m) override;

  llvm::PreservedAnalyses run(llvm::Module &m,
    llvm::AnalysisManager<llvm::Module>& am);
};
