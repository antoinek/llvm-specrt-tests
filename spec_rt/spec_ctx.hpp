#pragma once

#include <map>
#include <unordered_map>
#include <string>

#include "llvm/IR/Constants.h"
#include "llvm/IR/Value.h"

/* Abstract base class for specialization points. Ideally clients/applications
   should be able to just use this interface, as the points are created by the
   runtime. Currently not quite there yet. See TODO at the bottom of the class.
 */
class SpecPointBase {
  protected:
    std::string label_;
    bool instrumented;

    SpecPointBase(const std::string &lbl) : label_(lbl), instrumented(false) {}

  public:
    const std::string &label() const { return label_; }


    /** Is instrumentation for this point enabled? */
    bool instrEnabled() const { return instrumented; }

    virtual void instrEnable() {
      instrumented = true;
    }

    virtual void instrDisable() {
      instrumented = false;
    }

    /** reset instrumentation statistics to 0 */
    virtual void instrReset() = 0;


    /** clear specialization for this point */
    virtual void specDisable() = 0;

    /** If this point is specialized, return pointer to llvm value */
    virtual llvm::Value *specLLVMValue(llvm::LLVMContext &llvmCtx) const = 0;


    /* TODO:
        Add abstract iterators to access all values from the instrumentation,
        either the histogram, or the map, or whatver.

        begin(), end() for  the iterators. Iterator should yield value and
        frequency, also make sure iterator can represent an unknown value (with
        histogram we have a field for "other").

        add abstract specialize() call that can take the iterator (value).
    */
};

/* Dense range instrumentation: done inline with the code in a histogram */
class SpecPointRange : public SpecPointBase {
  public:
    uint8_t bits;
    bool sign;

    /* min & max of the range */
    uint64_t rangeMin;
    uint64_t rangeMax;

    /* Histogram, is an array of size max - min + 2, the 1 extra element at the
       end is used for values outside the range. */
    uint64_t *histogram;

    bool specialized;
    uint64_t specValue;

    SpecPointRange(const std::string &lbl, uint8_t bits_, bool sign_,
                   uint64_t rmin, uint64_t rmax) :
      SpecPointBase(lbl), bits(bits_), sign(sign_), rangeMin(rmin),
      rangeMax(rmax), histogram(nullptr), specialized(false)
    {
    }

    virtual ~SpecPointRange()
    {
      if (histogram)
        delete[] histogram;
    }

    virtual void instrEnable() {
      if (!histogram) {
        size_t sz = rangeMax - rangeMin + 2;
        histogram = new uint64_t[sz];
        memset(histogram, 0, sz * sizeof(uint64_t));
      }
      SpecPointBase::instrEnable();
    }

    virtual void instrReset() {
      if (!histogram)
        return;

      size_t hsize = rangeMax - rangeMin + 2;
      for (size_t i = 0; i < hsize; i++)
        histogram[i] = 0;
    }

    void specialize(uint64_t x) {
      specValue = x;
      specialized = true;
    }

    virtual void specDisable() override {
      specialized = false;
    }

    virtual llvm::Value *specLLVMValue(llvm::LLVMContext &llvmCtx) const override
    {
      if (!specialized)
        return nullptr;

      llvm::IntegerType *it = llvm::IntegerType::get(llvmCtx, bits);
      return llvm::ConstantInt::get (it, specValue, sign);
    }
};


/* General specialization point. Uses a C++ map, better for none-dense ranges of
   values but slower to instrument */
class SpecPointGeneral : public SpecPointBase {
  public:
    uint8_t bits;
    bool sign;

    std::unordered_map <uint64_t, uint64_t> stats;

    bool specialized;
    uint64_t specValue;

    SpecPointGeneral(const std::string &lbl, uint8_t bits_, bool sign_) :
      SpecPointBase(lbl), bits(bits_), sign(sign_), specialized(false)
    {
    }

    virtual void instrReset() {
      for (auto i: stats) {
        stats[i.first] = 0;
      }
    }

    void specialize(uint64_t x) {
      specValue = x;
      specialized = true;
    }

    virtual void specDisable() override {
      specialized = false;
    }

    virtual llvm::Value *specLLVMValue(llvm::LLVMContext &llvmCtx) const override
    {
      if (!specialized)
        return nullptr;

      llvm::IntegerType *it = llvm::IntegerType::get(llvmCtx, bits);
      return llvm::ConstantInt::get (it, specValue, sign);
    }
};


/* A context represents the entire instrumentation and specialization state for
   the system. */
class SpecCtx {
  public:
    std::map <std::string, SpecPointBase *> points;

    void addPoint(SpecPointBase &point) {
      points[point.label()] = &point;
    }

    SpecPointBase *getPoint(const std::string &name) {
      auto i = points.find(name);
      if (i == points.end())
        return nullptr;
      return i->second;
    }
};

extern "C" void __spec_general_instr(SpecPointGeneral *spg, uint64_t val);