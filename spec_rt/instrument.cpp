#include "llvm/Pass.h"
#include "llvm/IR/Attributes.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/raw_ostream.h"

#include <map>
#include <string>

#include "instrument.hpp"

using namespace llvm;

bool Instrument::rewriteInstrCall(Module &mod, Function &parent, CallInst &ci) {
  Function *cf = ci.getCalledFunction();
  if (!cf) {
    return false;
  }

  if (cf->getName().startswith("__instr")) {
    // all __instr calls have the same first two args: name and value

    // get the label string first (ugh...)
    auto *gep = dyn_cast<ConstantExpr> (ci.getArgOperand(0));
    auto *cs = gep->getOperand(0);
    auto *gv = dyn_cast<GlobalVariable> (cs);
    auto *gi = gv->getInitializer();
    auto *gic = dyn_cast<ConstantDataArray> (gi);
    StringRef name = gic->getAsString().rtrim('\0');

    // type of the second arg, the value to instrument
    auto *iArgOp = ci.getArgOperand(1);
    IntegerType *ivalTy = dyn_cast<IntegerType>(iArgOp->getType());


    // STEP 0: create replacement function
    std::string in_f_n = "__instr_fun_";
    in_f_n.append(name);
    FunctionCallee in_f_c = mod.getOrInsertFunction(in_f_n, cf->getFunctionType());
    Function *in_f = mod.getFunction(in_f_n);

    // add alwaysinline attribute
    in_f->addFnAttr(Attribute::AlwaysInline);
    in_f->setLinkage(Function::PrivateLinkage);

    // second argument, the value passed in
    Value *iArgV = in_f->getArg(1);

    // these function will comprise 3 basic blocks:
    //   1) instrumentation: record value (if instr enabled)
    //   2) spec-check: check for expected value (if spec enabled)
    //   3) spec-check fail: handle failed check (call into c++ handler)
    //   4) return: return either original input value or spec value
    BasicBlock* bInstr = BasicBlock::Create(in_f->getContext(), "entry", in_f);
    BasicBlock* bSpecCheck = BasicBlock::Create(in_f->getContext(), "spec_check", in_f);
    BasicBlock* bSpecFail = BasicBlock::Create(in_f->getContext(), "spec_fail", in_f);
    BasicBlock* bRet = BasicBlock::Create(in_f->getContext(), "ret", in_f);


    // STEP 1: Look up or create specialization point
    SpecPointBase *sp = specCtx->getPoint(name);
    if (!sp) {
      if (cf->getName().startswith("__instr_range")) {
        /* For range calls the next two args are min and max */
        auto *min_val_ex = dyn_cast<ConstantInt> (ci.getArgOperand(2));
        auto *max_val_ex = dyn_cast<ConstantInt> (ci.getArgOperand(3));

        bool sign = false; // TODO
        sp = new SpecPointRange(name, ivalTy->getBitWidth(), sign,
          min_val_ex->getZExtValue(), max_val_ex->getZExtValue());
      } else if (cf->getName().startswith("__instr_general")) {
        bool sign = false; // TODO
        sp = new SpecPointGeneral(name, ivalTy->getBitWidth(), sign);
      } else {
        errs() << "unsupported instrumentation call " << cf->getName() << "\n";
        abort();
      }
      specCtx->addPoint(*sp);
    }


    // STEP 2: generate instrumentation if enabled
    IRBuilder<> buildInstr(bInstr);
    if (sp->instrEnabled()) {
      if (cf->getName().startswith("__instr_range")) {
        SpecPointRange *pr = dynamic_cast<SpecPointRange *> (sp);
        if (!pr) {
          errs() << "spec point type does not match call name :-/\n";
          abort();
        }

        /* add simple histogram */
        IntegerType *u64 = IntegerType::get(in_f->getContext(), 64);
        //ArrayType::get(u64, pr->rangeMax - pr->rangeMin + 2)
        Value *ap = buildInstr.CreateIntToPtr(
          ConstantInt::get(u64, (uintptr_t) pr->histogram, false),
          PointerType::getUnqual(u64));
        Value *idx = buildInstr.CreateSub(iArgV,
          ConstantInt::get(ivalTy, pr->rangeMin, false));
        // TODO: check for out of range in which case we use the last element
        Value *ptr = buildInstr.CreateGEP(u64, ap, ArrayRef<Value *>(idx));

        // TODO: should probably be atomic fetch and add
        Value *old = buildInstr.CreateLoad(u64, ptr, false);
        Value *newval = buildInstr.CreateAdd(old,
          ConstantInt::get(u64, 1, false));
        buildInstr.CreateStore(newval, ptr);
      } else if (cf->getName().startswith("__instr_general")) {
        SpecPointGeneral *pg = dynamic_cast<SpecPointGeneral *> (sp);
        if (!pg) {
          errs() << "spec point type does not match call name :-/\n";
          abort();
        }

        /* add call to c++ */
        Function *fSPGeneral = mod.getFunction("__spec_general_instr");
        Value *handle = ConstantInt::get(IntegerType::get(mod.getContext(), 64),
          (uintptr_t) pg, false);
        Value * args[2] = {handle, iArgV};
        buildInstr.CreateCall(fSPGeneral->getFunctionType(), fSPGeneral,
          ArrayRef <Value *>(args, 2));
      } else {
        errs() << "unsupported instrumentation point type\n";
        abort();
      }
    }
    buildInstr.CreateBr(bSpecCheck);
    

    // STEP 3: do specialization if value is set
    IRBuilder<> buildCheck(bSpecCheck);
    IRBuilder<> buildFail(bSpecFail);
    Value *retVal = iArgV; // default to return arg value
    Value *specVal = sp->specLLVMValue(mod.getContext());
    if (specVal) {
      // specialization enabled

      // return specialized value
      retVal = specVal;

      // add safety check
      Value *check = buildCheck.CreateICmpEQ(iArgV, retVal);
      buildCheck.CreateCondBr(check, bRet, bSpecFail);

      // generate check fail call (in c++ code)
      Function *fCkfail = mod.getFunction("__spec_check_fail");
      Value *handle = ConstantInt::get(IntegerType::get(mod.getContext(), 64),
        (uintptr_t) sp, false);
      Value * args[2] = {handle, iArgV};
      buildFail.CreateCall(fCkfail->getFunctionType(), fCkfail,
        ArrayRef <Value *>(args, 2));
    } else {
      buildCheck.CreateBr(bRet);
    }
    buildFail.CreateBr(bRet);


    // STEP4 3: assemble return basic block
    IRBuilder<> builder(bRet);
    builder.CreateRet(retVal);
    
    // replace instrument call with new function
    ci.setCalledOperand(in_f_c.getCallee());
    return true;
  }

  return false;
}

bool Instrument::runOnFunction(Module &m, Function &f) {
  bool changed = false;
  for (BasicBlock &bb: f) {
    for (Instruction &i: bb) {
      if (auto *ci = dyn_cast <CallInst> (&i)) {
        if (rewriteInstrCall(m, f, *ci))
          changed = true;
      }
    }
  }
  return changed;
}

bool Instrument::runOnModule(Module &m) {
  // add prototype for __spec_check_fail function
  IntegerType *u64 = IntegerType::get(m.getContext(), 64);
  Type *its[2] = {u64, u64};
  FunctionType *ft = FunctionType::get(Type::getVoidTy(m.getContext()),
    ArrayRef<Type *>(its, 2), false);
  m.getOrInsertFunction("__spec_check_fail", ft);
  Function *in_f = m.getFunction("__spec_check_fail");
  in_f->addFnAttr(Attribute::NoReturn);

  // add prototype for __spec_general_instr function
  ft = FunctionType::get(Type::getVoidTy(m.getContext()),
    ArrayRef<Type *>(its), false);
  m.getOrInsertFunction("__spec_general_instr", ft);

  bool updated = false;
  for (auto &f: m.functions())
    if (runOnFunction(m, f))
      updated = true;
  return updated;
}


PreservedAnalyses Instrument::run(Module &m, AnalysisManager<Module>& am)
{
  if (runOnModule(m))
    return PreservedAnalyses();
  else
    return PreservedAnalyses::all();
}

StringRef Instrument::name() {
  return "instrument";
}

char Instrument::ID = 0;
static RegisterPass<Instrument> X("instrument", "Instrument Pass",
                             false /* Only looks at CFG */,
                             false /* Analysis Pass */);
