#include "spec_ctx.hpp"

void __spec_general_instr(SpecPointGeneral *spg, uint64_t val)
{
    auto i = spg->stats.find(val);
    if (i == spg->stats.end()) {
        spg->stats[val] = 1;
    } else {
        spg->stats[val] += 1;
    }
}