#pragma once

#include <string>
#include "spec_ctx.hpp"

#include "llvm/ExecutionEngine/Orc/LLJIT.h"


class SpecRuntime {
  protected:
    struct Instance {
      std::unique_ptr<llvm::orc::LLJIT> jit;
      llvm::orc::ThreadSafeContext *tsctx;
    };

    std::string path;
    Instance *i;

  public:
    SpecCtx ctx;
    bool verbose;

    SpecRuntime(const std::string &path_);

    /* runs initial compilation which also fills in the context with all
       specialization points */
    void init();

    /* re-compile (call after making changes to the ctx) */
    void update();

    /* get pointer to function with specified name */
    void *getFunction(const std::string &fnName);
};

extern "C" void __spec_check_fail(SpecPointBase *e, uint64_t val);
